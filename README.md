# Temat
Techniki statycznej weryfikacji i walidacji oprogramowania.

# Cel
Celem zajęć jest wprowadzenie do statycznych technik weryfikacji i walidacji oprogramowania. W ramach ćwiczeń zostanie wykonany przegląd dokumentacji i kodu w języku Java/Python/TypeScript.

# Zadania

Praca indywidualna lub w grupach. Każda osoba (lub grupa) wybiera jeden pull request z dostępnych i wpisuje nazwiska w opisie.

## Zadanie 1 – Przegląd dokumentacji
W ramach zadania należy przygotować przegląd dokumentacji: opisu oraz specyfikacji projektu informatycznego. Celem przeglądu jest wykrycie przede wszystkim typowych wad specyfikacji, np.: niespójności, niekompletnych i nieweryfikowalnych wymagań, zapisów ryzykownych, zapisów niezgodnych z dobrymi praktykami.

W ramach przeglądu należy przygotować listę punktów, które wymagają modyfikacji, poprawy lub wyjaśnienia, w formie komentarzy do pull requesta.

Pull requesty do wyboru: https://gitlab.com/spio/03static/-/merge_requests

## Zadanie 2 – Przegląd kodu
W ramach zadania należy przygotować przegląd kodu. Celem przeglądu jest znalezienie ewentualnych błędów w kodzie oraz sprawdzenie kodu względem dobrych praktyk programistycznych. Należy zwrócić uwagę na czytelność, zrozumiałość, zarządzalność oraz pielęgnowalność kodu.

W ramach przeglądu należy przygotować listę uwag wewnątrz pull requesta, jako komentarze do linijek kodu.

Pull requesty do wyboru: https://gitlab.com/Martenka/intervals/-/merge_requests